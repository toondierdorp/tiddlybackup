import os
import sys
import time
import json 
import datetime
from threading import Timer


class RepeatedTimer(object):
    def __init__(self, interval, function, *args, **kwargs):
        self._timer     = None
        self.interval   = interval
        self.function   = function
        self.args       = args
        self.kwargs     = kwargs
        self.is_running = False
        self.start()

    def _run(self):
        self.is_running = False
        self.start()
        self.function(*self.args, **self.kwargs)

    def start(self):
        if not self.is_running:
            self._timer = Timer(self.interval, self._run)
            self._timer.start()
            self.is_running = True

    def stop(self):
        self._timer.cancel()
        self.is_running = False

with open('TiddlyBackup.json','r') as fp:
    config = json.load(fp)

favicon = config['favicon']
file_name = config['file_name']
file_location = config['file_location']
backup_location = config['backup_location']
download_location = config['download_location']

def backup(file_name, file_location, backup_location):
    t = datetime.datetime.now()
    date = t.strftime('%Y-%m-%d-%H%M%S')
    if os.path.exists(file_location+file_name):
        old_file = os.path.join(file_location, file_name)
        new_file = os.path.join(backup_location, date+'-'+file_name)
        os.rename(old_file, new_file)


def move(file_name, file_location, download_location):
    if os.path.exists(download_location+file_name):
        old_file = os.path.join(download_location, file_name)
        new_file = os.path.join(file_location, file_name)
        os.rename(old_file, new_file)        

def run_file_backup(favicon, file_name, file_location, backup_location, download_location):

    if os.path.exists(download_location+file_name): 
        #print("Tiddly Download detected") # debug
        

        if os.path.exists(file_location+file_name):
            #print("Tiddly Backup made") # debug
            backup(file_name, file_location, backup_location)


        if os.path.exists(download_location+file_name):
            #print("Tiddly Moved") # debug
            move(file_name, file_location, download_location)
            

rt = RepeatedTimer(10, run_file_backup, favicon, file_name, file_location, backup_location, download_location)