A small threaded Python script for automatically backing up a tiddlywiki on save. 

This script check if a file with a specific name is found in the download directory.
If has found something it backup up the exsisting file and copies over the new 
downloaded file the the working directory.

In `TiddlyBackup.json` are some setting that are important. 

`file_name` is the file name of your tiddlywiki
`file_location` is the location of where you want the tiddlywiki file to open from. 
`backup_location` is where the previous versions will be if something goes wrong.
`download_location` is the location you need to download to for the detection to 
work.

With pyinstaller you can make a onefile exe file. 
`pyinstaller --onefile --noconsole TiddlyBackup.py`

Put the executable together with the favicon and json file in a directory and 
run it.